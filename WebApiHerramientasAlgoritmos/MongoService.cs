﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using WebApiHerramientasAlgoritmos.Entidades;

namespace WebApiHerramientasAlgoritmos
{
    public class MongoService
    {
        bool resultado;
        MongoClient client;
        IMongoDatabase db;
        public MongoService()
        {
            client = new MongoClient(new MongoUrl("mongodb+srv://imfe:wKktoMkiuBYQR2yQ@sensores.hkgmh.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"));
            db = client.GetDatabase("Sensores");
        }

        public IMongoCollection<Lectura> Collection() => db.GetCollection<Lectura>(typeof(Lectura).Name);

        public string Error { get; private set; }

        public IEnumerable<Lectura> Read
        {
            get
            {
                var r = Collection().AsQueryable();
                foreach (var item in r)
                {
                    item.FechaHora = item.FechaHora.ToLocalTime();
                }
                return r;
            }
        }

        

        public IEnumerable<Lectura> AddMultiple(IEnumerable<Lectura> entidades)
        {
            List<Lectura> noInsertadas = new List<Lectura>();
            string errores = "";
            foreach (var entidad in entidades)
            {
                if (Create(entidad) == null)
                {
                    noInsertadas.Add(entidad);
                    errores += Error;
                }
            }
            Error = errores;
            return noInsertadas;
        }

        public Lectura Create(Lectura entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            entidad.FechaHora = DateTime.Now.AddHours(-5);
            try
            {
                Collection().InsertOne(entidad);
                Error = "";
                resultado = true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                resultado = false;
            }
            return resultado?entidad:null;
        }

        public bool Delete(string id)
        {
            try
            {
                int r = (int)Collection().DeleteOne(e => e.Id == id).DeletedCount;
                resultado = r == 1;
                Error = resultado ? "" : $"Se eliminaron {r} elementos";
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                resultado = false;
            }
            return resultado;
        }


        public IEnumerable<Lectura> Query(Expression<Func<Lectura, bool>> predicado)
        {

            var r = Read.Where(predicado.Compile());
            foreach (var item in r)
            {
                item.FechaHora = item.FechaHora.ToLocalTime();
            }
            return r;
        }

        public Lectura SearchById(string id)
        {
            var r = Query(e => e.Id == id).SingleOrDefault();
            r.FechaHora = r.FechaHora.ToLocalTime();
            return r;
        }

        public Lectura Update(Lectura entidad)
        {
            entidad.FechaHora = DateTime.Now.AddHours(-5);

            try
            {
                int r = (int)Collection().ReplaceOne(e => e.Id == entidad.Id, entidad).ModifiedCount;
                resultado = r == 1;
                Error = resultado ? "" : $"Se modificaron {r} elementos";
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                resultado = false;
            }
            return resultado ? entidad : null;
        }
    }
}
