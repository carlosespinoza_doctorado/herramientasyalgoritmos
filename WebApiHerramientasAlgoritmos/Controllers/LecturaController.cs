﻿using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApiHerramientasAlgoritmos.Entidades;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApiHerramientasAlgoritmos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LecturaController : ControllerBase
    {
        MongoService service;

        public LecturaController(MongoService service)
        {
            this.service = service;
        }

        [HttpGet("TotalPorUbicacion")]
        public ActionResult<Dictionary<string, long>> TotalPorUbicacion()
        {
            Dictionary<string, long> datos = new Dictionary<string, long>();
            var ubicaciones = service.Collection().Distinct<string>("Ubicacion", MongoDB.Driver.FilterDefinition<Lectura>.Empty).ToList();
            foreach (var item in ubicaciones)
            {
                datos.Add(item, service.Collection().CountDocuments(p => p.Ubicacion == item));
            }
            return Ok(datos);
        }

        [HttpGet("Total")]
        public ActionResult<int> Total()
        {
            return Ok(service.Collection().CountDocuments(FilterDefinition<Lectura>.Empty));

        }

        [HttpGet("UltimosEnIntervalo/{horas}")]
        public ActionResult<List<Lectura>> UltimosEnIntervalo(int horas)
        {
            FilterDefinitionBuilder<Lectura> filtro=new FilterDefinitionBuilder<Lectura>();
            DateTime fecha = DateTime.Now.AddHours(-5+(-1*horas));
            var f = filtro.Gte("FechaHora", fecha);
           // var datos = service.Collection().Find(l => l.FechaHora >= fecha).ToList();
            return Ok(service.Collection().Find(f).ToList());
        }

        [HttpGet("Ultimos")]
        public ActionResult<List<Lectura>> Ultimos()
        {
            List<Lectura> datos = new List<Lectura>();

            //DateTime inicio = DateTime.Now;
            var personas = service.Collection().Distinct<string>("Integrante", MongoDB.Driver.FilterDefinition<Lectura>.Empty).ToList();
            FilterDefinitionBuilder<Lectura> filtro;
            foreach (var item in personas)
            {
                filtro = new FilterDefinitionBuilder<Lectura>();
                var f = filtro.Eq("Integrante", item);
                datos.Add(service.Collection().Find(f).SortByDescending(f => f.FechaHora).First());
            }
            return Ok(datos);
            //TimeSpan duracion = DateTime.Now - inicio;
            //inicio = DateTime.Now;
            //var todos=service.Read.ToList();
            //var integrantes = todos.Select(p => p.Integrante).Distinct();
            //foreach (var integrante in integrantes)
            //{
            //    datos.Add(todos.Where(i => i.Integrante == integrante).OrderBy(f => f.FechaHora).Last());
            //}
            //duracion = DateTime.Now - inicio;
        }


        // GET: api/<LecturaController>
        [HttpGet]
        public ActionResult<IEnumerable<Lectura>> Get()
        {
            return Ok(service.Read);
        }

        // GET api/<LecturaController>/5
        [HttpGet("{id}")]
        public ActionResult<Lectura> Get(string id)
        {
            try
            {
                return Ok(service.SearchById(id));
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/<LecturaController>
        [HttpPost]
        public ActionResult<Lectura> Post([FromBody] Lectura value)
        {
            try
            {

                return Ok(service.Create(value));
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<LecturaController>/5
        [HttpPut]
        public ActionResult<Lectura> Put([FromBody] Lectura value)
        {
            try
            {
                return Ok(service.Update(value));
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE api/<LecturaController>/5
        [HttpDelete("{id}")]
        public ActionResult<bool> Delete(string id)
        {
            try
            {
                return Ok(service.Delete(id));
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
