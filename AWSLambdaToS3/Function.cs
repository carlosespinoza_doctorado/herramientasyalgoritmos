using Amazon.Lambda.Core;
using System.Net.Http;
using Newtonsoft.Json;
using AWSLambdaToS3.Entidades;
using System.Text;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

//https://www.c-sharpcorner.com/article/write-serverless-functions-using-aws-lambda-and-c-sharp/
//https://www.c-sharpcorner.com/blogs/working-with-files-and-folders-in-s3-using-aws-sdk-for-net
//https://www.c-sharpcorner.com/article/how-to-create-files-in-aws-lambda-tmp-directory-using-c-sharp/
//https://stackoverflow.com/questions/3863477/how-to-set-the-permission-on-files-at-the-time-of-upload-through-amazon-s3-api
//https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/RunLambdaSchedule.html

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace AWSLambdaToS3;
/// <summary>
/// Funci�n AWS Lambda que permite obtener datos de la API y almacenarlos en varios CSV en un Bucket de AWS S3
/// </summary>
public class Function
{
    HttpClient client;
    AmazonS3Client amazonS3;
    string uriApi;
    string Error;
    string bucketName = "cespinozaimfe";
    string awsAccessKey = "AKIA46KXQKKQYIVV2GOT";
    string awsSecretKey = "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";
    string carpetaS3 = @"Lecturas/";

    /// <summary>
    /// Invocaci�n de la funci�n
    /// </summary>
    /// <param name="context">Context de Lambda Function</param>
    /// <returns></returns>
    //public string FunctionHandler(string input, ILambdaContext context)
    public string FunctionHandler(ILambdaContext context)
    {
        Console.WriteLine("Proceso...");
        var ultimas =ObtenerUltimas;
        Procesar(ultimas, "ultimas.csv");
        Procesar(ObtenerUltimaHora, "ultimaHora.csv");
        Procesar(ObtenerTotalesPorUbicacion, "totalPorUbicacion.csv");
        Procesar(ObtenerTotal, "total.csv");
        Procesar(ObtenerTodas, "todas.csv");
        return "Completado";
    }
    
    private void Procesar(int datos,string nomArchivo)
    {
        if (datos >= 0)
        {
            Console.WriteLine("Procesando " + nomArchivo);
            Console.WriteLine("Convirtiendo a CSV");
            string csv = ConvertirListToCsv(datos);
            Console.WriteLine("Convertido, guardando en S3...");
            GuardarEnS3(nomArchivo, csv).Wait();
            Console.WriteLine("Archivo creado correctamente!");
        }
        else
        {
            Console.WriteLine("Error al obtener datos: " + Error);
        }
    }

    private string ConvertirListToCsv(int datos)
    {
        StringBuilder texto = new StringBuilder();
        texto.AppendLine("Dato,Total");
        texto.AppendLine($"Total de registros,{datos}");
        Console.WriteLine("Registros: 1");
        return texto.ToString();
    }

    private void Procesar(Dictionary<string,int> datos, string nomArchivo)
    {
        if (datos != null)
        {
            Console.WriteLine("Procesando " + nomArchivo);
            Console.WriteLine("Convirtiendo a CSV");
            string csv = ConvertirListToCsv(datos);
            Console.WriteLine("Convertido, guardando en S3...");
            GuardarEnS3(nomArchivo, csv).Wait();
            Console.WriteLine("Archivo creado correctamente!");
        }
        else
        {
            Console.WriteLine("Error al obtener datos: " + Error);
        }
    }

    private string ConvertirListToCsv(Dictionary<string, int> datos)
    {
        StringBuilder texto = new StringBuilder();
        texto.AppendLine("Ubicacion,Total");
        foreach (var item in datos)
        {
            texto.AppendLine($"{item.Key},{item.Value}");
        }
        Console.WriteLine("Registros: " + datos.Count);
        return texto.ToString();
    }

    private void Procesar(List<Lectura> datos, string nomArchivo)
    {
        if (datos != null)
        {
            Console.WriteLine("Procesando " + nomArchivo);
            Console.WriteLine("Convirtiendo a CSV");
            string csv = ConvertirListToCsv(datos);
            Console.WriteLine("Convertidas, guardando en S3...");
            GuardarEnS3(nomArchivo, csv).Wait();
            Console.WriteLine("Archivo creado correctamente!");
        }
        else
        {
            Console.WriteLine("Error al obtener datos: " + Error);
        }
    }


    private async Task GuardarEnS3(string nomArchivo, string contenido)
    {
        string rutaArchivo = carpetaS3 + nomArchivo;
        string rutaLocal = @"/tmp/" + nomArchivo;
        using (FileStream fs = System.IO.File.Create(rutaLocal))
        {
            byte[] info = new UTF8Encoding(true).GetBytes(contenido);
            fs.Write(info, 0, info.Length);
        }
        FileInfo fi= new FileInfo(rutaLocal);
        if (fi.Exists)
        {
            Console.WriteLine("Archivo local creado " + rutaLocal);
            var fileTranferUtility = new TransferUtility(amazonS3);
            try
            {
                Console.WriteLine("Escribiendo archivo " + rutaArchivo);
                await fileTranferUtility.UploadAsync(rutaLocal,bucketName,rutaArchivo);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            fi.Delete();
            Console.WriteLine("Archivo local ELIMINADO " + rutaLocal);
        }
        
    }

    private string ConvertirListToCsv(List<Lectura> lista)
    {
        StringBuilder contenido = new StringBuilder();
        contenido.AppendLine("Id,FechaHora,Integrante,Ubicacion,Latitud,Longitud,Temperatura,Humedad,Luminosidad");
        int i = 0;
        foreach (var item in lista)
        {
            contenido.AppendLine($"{item.Id},{item.FechaHora},{item.Integrante},{item.Ubicacion},{item.Latitud},{item.Longitud},{item.Temperatura},{item.Humedad},{item.Luminosidad}");
            i++;
        }
        Console.WriteLine("Registros en tabla: " + i);
        return contenido.ToString();
    }

    public Function()
    {
        client = new HttpClient();
        client.BaseAddress = new Uri(@"http://apiherramientasalgortimos.azurewebsites.net/");
        client.DefaultRequestHeaders.Accept.Clear();
        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        amazonS3 = new AmazonS3Client(awsAccessKey, awsSecretKey, Amazon.RegionEndpoint.USEast1);
        uriApi = "";
        Error = "";
    }
    #region M�todos Cl�sicos

    public List<Lectura> ObtenerTodas => ObtenerTodasAsync().Result;
    public List<Lectura> ObtenerUltimas => ObtenerUltimasAsync().Result;
    public List<Lectura> ObtenerUltimaHora => ObtenerUltimaHoraAsync().Result;
    public Dictionary<string, int> ObtenerTotalesPorUbicacion => ObtenerTotalesPorUbicacionAsync().Result;

    public int ObtenerTotal=>ObtenerTotalAsync().Result;
    #endregion



    #region M�todos As�ncronos

    private async Task<List<Lectura>> ObtenerTodasAsync()
    {
        uriApi = $"api/Lectura";
        try
        {
            HttpResponseMessage response = await client.GetAsync(uriApi).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var respuesta = JsonConvert.DeserializeObject<List<Lectura>>(content);
                Error = "";
                return respuesta;
            }
            else
            {
                Error = "Error al obtener los datos";
                return null;
            }
        }
        catch (Exception ex)
        {
            Error = ex.Message;
            return null;
        }
    }

    private async Task<int> ObtenerTotalAsync()
    {
        uriApi = $"api/Lectura/Total";
        try
        {
            HttpResponseMessage response = await client.GetAsync(uriApi).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                Error = "";
                return int.Parse(content);
            }
            else
            {
                Error = "Error al obtener los datos";
                return -1;
            }
        }
        catch (Exception ex)
        {
            Error = ex.Message;
            return -1;
        }
    }

    private async Task<Dictionary<string,int>> ObtenerTotalesPorUbicacionAsync()
    {
        uriApi = $"api/Lectura/TotalPorUbicacion";
        try
        {
            HttpResponseMessage response = await client.GetAsync(uriApi).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var respuesta = JsonConvert.DeserializeObject<Dictionary<string,int>>(content);
                Error = "";
                return respuesta;
            }
            else
            {
                Error = "Error al obtener los datos";
                return null;
            }
        }
        catch (Exception ex)
        {
            Error = ex.Message;
            return null;
        }
    }


    private async Task<List<Lectura>> ObtenerUltimaHoraAsync()
    {
        uriApi = $"api/Lectura/UltimosEnIntervalo/1";
        try
        {
            HttpResponseMessage response = await client.GetAsync(uriApi).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var respuesta = JsonConvert.DeserializeObject<List<Lectura>>(content);
                Error = "";
                return respuesta;
            }
            else
            {
                Error = "Error al obtener los datos";
                return null;
            }
        }
        catch (Exception ex)
        {
            Error = ex.Message;
            return null;
        }
    }

    private async Task<List<Lectura>> ObtenerUltimasAsync()
    {
        uriApi = $"api/Lectura/Ultimos";
        try
        {
            HttpResponseMessage response = await client.GetAsync(uriApi).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var content=await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var respuesta=JsonConvert.DeserializeObject<List<Lectura>>(content);
                Error = "";
                return respuesta;
            }
            else
            {
                Error = "Error al obtener los datos";
                return null;
            }
        }
        catch (Exception ex)
        {
            Error = ex.Message;
            return null;
        }
    }
    #endregion
}
