﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWSLambdaToS3.Entidades
{
    public class Lectura
    {
        public string Id { get; set; }
        public DateTime FechaHora { get; set; }
        public string Integrante { get; set; }
        public string Ubicacion { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public float Temperatura { get; set; }
        public float Humedad { get; set; }
        public float Luminosidad { get; set; }
    }
}
