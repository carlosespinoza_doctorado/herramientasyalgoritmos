/*
 Name:		v2ConAWSDynaboDb.ino
 Created:	08/05/2022 09:13:00 am
 Author:	Mtro. Tawny Nava Garcia, Mtro. Carlos Espinoza y Mtro. Alejandro Ulises
*/
#include "secrets.h"
#include "WiFi.h"
#include "ThingSpeak.h"
#include "WiFi.h"
#include "DHT.h"
#include "Adafruit_BMP085.h"
#include <HTTPClient.h>
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>


#define ldr		34    //Pin Photoresistencia
#define pinDHT	13	  //Pin DHT11
#define ledNoWiFi  22
#define ledWiFi    23

DHT dht1(pinDHT, DHT11);
Adafruit_BMP085 bmp;
WiFiClient cliente;
WiFiClientSecure net = WiFiClientSecure();
PubSubClient client(net);

//Variables
int luminosidad = 0;
float temp = 0;
float hum = 0;

void setup() {
	Serial.begin(115200);
	pinMode(ledNoWiFi, OUTPUT);
	pinMode(ledWiFi, OUTPUT);
	ConectarWiFi();
	ConfiguraAWSIoTCore();
	ThingSpeak.begin(cliente);
}

void ConfiguraAWSIoTCore() {
	//Configura WiFiClientSecure para usar las credenciales del dispositio AWS IoT
	net.setCACert(AWS_CERT_CA);
	net.setCertificate(AWS_CERT_CRT);
	net.setPrivateKey(AWS_CERT_PRIVATE);

	// Conectándose al agente MQTT en el punto final de AWS que definimos anteriormente
	client.setServer(AWS_IOT_ENDPOINT, 8883);

	// Creando un controlador de mensaje
	client.setCallback(messageHandler);

	Serial.println("Conectándose a AWS IOT");

	while (!client.connect(THINGNAME))
	{
		Serial.print(".");
		delay(100);
	}

	if (!client.connected())
	{
		Serial.println("Tiempo de espera de AWS IoT!");
		return;
	}

	// Subscribiendose al tópico o tema
	client.subscribe(AWS_IOT_SUBSCRIBE_TOPIC);

	Serial.println("Conectado a AWS IoT");
}

void ConectarWiFi() {
	Serial.println("Conectando a " + String(ssid));
	WiFi.begin(ssid, password);
	digitalWrite(ledNoWiFi, HIGH);
	digitalWrite(ledWiFi, LOW);
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}
	Serial.println("Wifi conectado!");
	digitalWrite(ledNoWiFi, LOW);
	digitalWrite(ledWiFi, HIGH);
}

void loop() {
	if (WiFi.status() != WL_CONNECTED) {
		ConectarWiFi();
	}
	client.loop();
	LeerSensores();
	ImprimirDatosDeSensores();
	CargarDatos();
	publishMessage();
	delay(25000);
}

void ImprimirDatosDeSensores() {
	Serial.println("Luminosidad: " + String(luminosidad) + " %");
	Serial.println("Temperatura: " + String(temp) + " C");
	Serial.println("Humedad: " + String(hum) + " %");
}

void LeerSensores() {
	luminosidad = map(analogRead(ldr), 0, 4095, 0, 100);
	temp = dht1.readTemperature();
	hum = dht1.readHumidity();

	while (isnan(temp) || isnan(hum)) {
		Serial.println("Lectura fallida en el sensor DHT11, repitiendo lectura...");
		delay(2000);
		temp = dht1.readTemperature();
		hum = dht1.readHumidity();
	}
}

void CargarDatos() {
	CargarATinkSpeak();
	InsertarLectura();
}


bool InsertarLectura() {
	HTTPClient http;
	String path = urlServidor + "api/Lectura";
	String json = "{\"integrante\": \"" + integrante + "\",\"ubicacion\": \"" + ubicacion + "\",\"latitud\": \"" + latitud + "\",\"longitud\": \"" + longitud + "\",\"temperatura\": " + String(temp) + ",\"humedad\": " + String(hum) + ",\"luminosidad\": " + String(luminosidad) + "}";
	http.begin(path);
	http.addHeader("Content-Type", "application/json");
	int httpResponseCode = http.POST(json);
	String respuesta = http.getString();
	http.end();
	if (httpResponseCode == 200) {
		Serial.println("Datos enviados correctamente a la API");
	}
	else {
		Serial.println("Error al cargar los datos a la API");
		Serial.println(respuesta);
	}
}

void CargarATinkSpeak() {
	ThingSpeak.setField(1, temp);
	ThingSpeak.setField(2, hum);
	ThingSpeak.setField(3, numIntegrante);
	ThingSpeak.setField(4, luminosidad);
	ThingSpeak.writeFields(channelID, WriteAPIKey);
	Serial.println("Datos enviados a ThingSpeak!");
}

void publishMessage()
{
	StaticJsonDocument<200> doc;
	doc["numintegrante"] = numIntegrante;
	doc["integrante"] = integrante;
	doc["ubicacion"] = ubicacion;
	doc["latitud"] = latitud;
	doc["longitud"] = longitud;
	doc["luminosidad"] = luminosidad;
	doc["humedad"] = hum;
	doc["temperatura"] = temp;

	char jsonBuffer[512];
	serializeJson(doc, jsonBuffer); // print to client

	client.publish(AWS_IOT_PUBLISH_TOPIC, jsonBuffer);
}

void messageHandler(char* topic, byte* payload, unsigned int length)
{
	Serial.print("Conectando: ");
	Serial.println(topic);
	StaticJsonDocument<200> doc;
	deserializeJson(doc, payload);
	const char* message = doc["message"];
	Serial.println(message);
}