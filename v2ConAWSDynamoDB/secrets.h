#include <pgmspace.h>

#define SECRET
#define THINGNAME "dht11_esp32_imfe6"                      

const char AWS_IOT_ENDPOINT[] = "aaceyxsq3bb2-ats.iot.us-east-1.amazonaws.com";      

String urlServidor = "http://apiherramientasalgortimos.azurewebsites.net/"; //URL de la API
unsigned long channelID = 1612330;         //ID canal ThingSpeak
const char* WriteAPIKey = "9JNO13X466B2UAGB";   //Key ThingSpeak

#define AWS_IOT_PUBLISH_TOPIC     "device/6/data"  //"esp32/pub" //donde se publicaran los datos
#define AWS_IOT_SUBSCRIBE_TOPIC   "device/6/data" //"esp32/sub" //donde enviará una notificación al esp32

String ubicacion = "Huichapan";   //Colocar el nombre de la localidad donde estan
String latitud = "20.371620";     //Colocar cordenadas GPS de la localidad
String longitud = "-99.645469";
String integrante = "Carlos Espinoza";  //Colocar el nombre del integrante
int numIntegrante = 6;
const char* ssid = "Hulux-MH412";       //Colocar el nombre de la RED Wifi       
const char* password = "w2hdE9VRsikc";  //Colocar la clave de la red wifi



// Amazon Root CA 1
static const char AWS_CERT_CA[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIDQTCCAimgAwIBAgITBmyfz5m/jAo54vB4ikPmljZbyjANBgkqhkiG9w0BAQsF
ADA5MQswCQYDVQQGEwJVUzEPMA0GA1UEChMGQW1hem9uMRkwFwYDVQQDExBBbWF6
b24gUm9vdCBDQSAxMB4XDTE1MDUyNjAwMDAwMFoXDTM4MDExNzAwMDAwMFowOTEL
MAkGA1UEBhMCVVMxDzANBgNVBAoTBkFtYXpvbjEZMBcGA1UEAxMQQW1hem9uIFJv
b3QgQ0EgMTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALJ4gHHKeNXj
ca9HgFB0fW7Y14h29Jlo91ghYPl0hAEvrAIthtOgQ3pOsqTQNroBvo3bSMgHFzZM
9O6II8c+6zf1tRn4SWiw3te5djgdYZ6k/oI2peVKVuRF4fn9tBb6dNqcmzU5L/qw
IFAGbHrQgLKm+a/sRxmPUDgH3KKHOVj4utWp+UhnMJbulHheb4mjUcAwhmahRWa6
VOujw5H5SNz/0egwLX0tdHA114gk957EWW67c4cX8jJGKLhD+rcdqsq08p8kDi1L
93FcXmn/6pUCyziKrlA4b9v7LWIbxcceVOF34GfID5yHI9Y/QCB/IIDEgEw+OyQm
jgSubJrIqg0CAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMC
AYYwHQYDVR0OBBYEFIQYzIU07LwMlJQuCFmcx7IQTgoIMA0GCSqGSIb3DQEBCwUA
A4IBAQCY8jdaQZChGsV2USggNiMOruYou6r4lK5IpDB/G/wkjUu0yKGX9rbxenDI
U5PMCCjjmCXPI6T53iHTfIUJrU6adTrCC2qJeHZERxhlbI1Bjjt/msv0tadQ1wUs
N+gDS63pYaACbvXy8MWy7Vu33PqUXHeeE6V/Uq2V8viTO96LXFvKWlJbYK8U90vv
o/ufQJVtMVT8QtPHRh8jrdkPSHCa2XV4cdFyQzR1bldZwgJcJmApzyMZFo6IQ6XU
5MsI+yMRQ+hDKXJioaldXgjUkK642M4UwtBV8ob2xJNDd2ZhwLnoQdeXeGADbkpy
rqXRfboQnoZsG4q5WTP468SQvvG5
-----END CERTIFICATE-----
)EOF";

// Device Certificate                                              
static const char AWS_CERT_CRT[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVALQ9rYikQnGxMI/dlvv3PvfbfMtIMA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0yMjA0MjcwMjU3
MzJaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDCW0u60k8nGIOLgilW
PYgm6QmqNW7kVzMrHICo5pJUM2rXI9LZ9ukCVwNbQTBdBHh0DwEKb9GjnZ1AQlEK
cXbelLEEt3b8TV9I1eXkdC/4gQ9ncHxePjOO3Xpchu5CBmTfqVNuhLErJL6R+/CG
qaFSFKVG2ippgHtEs56dtv3/GLjnwGmg91+hs9tmQ+J4+x1sRr/HY0BUGL8mXw4F
7Xp1xEEoORoRQGSwAYfyma6B4lyO3iH1JVEzjdvkzXZR3I9sj8SKgrjqzc9tfvat
GyPm/5k3z1vLPkSn/CpWCddbs1nR8azVoXBWvg1SYZXGWHBrI7vBgOVdxkuDwizF
O6fXAgMBAAGjYDBeMB8GA1UdIwQYMBaAFCegGr5O/KlJ3xZeucPMYejyfkwxMB0G
A1UdDgQWBBQZgxqPuI82Dky4x3Oz5MFR8ASh1TAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAOJv6NPzp69PUvNxNzn97YbkC
2ND9ppzmwmgKm6jGRMidbGTK41A5kzZwGqm2MMm5F/U8RJ2Y4+SfEyoNs3lCINZP
iql7LV4h8+fW9a26/uUzq/Zvl13h36+pDEI+00Xxh0Xo1grcRtV5d0GRCrtzCA6I
GWdAKkrRATqsnPYwwphGPTKvd1yx5mhmJNDWZ9NBCygB+aToDhKOUWMknrmZBSec
pees0YECMF4CALImmM4frSU3Z9MLl902PMO8KYeRD90hmZ0lWQ/E7F4Dlpig4bkw
1geMeRJGWNx7KYKgUfgUn/Q/Sj4fMrPbUtjwhOKVfHQrxxh6z+12xq2I9MHtsA==
-----END CERTIFICATE-----
 
 
)KEY";

// Device Private Key                                              
static const char AWS_CERT_PRIVATE[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAwltLutJPJxiDi4IpVj2IJukJqjVu5FczKxyAqOaSVDNq1yPS
2fbpAlcDW0EwXQR4dA8BCm/Ro52dQEJRCnF23pSxBLd2/E1fSNXl5HQv+IEPZ3B8
Xj4zjt16XIbuQgZk36lTboSxKyS+kfvwhqmhUhSlRtoqaYB7RLOenbb9/xi458Bp
oPdfobPbZkPiePsdbEa/x2NAVBi/Jl8OBe16dcRBKDkaEUBksAGH8pmugeJcjt4h
9SVRM43b5M12UdyPbI/EioK46s3PbX72rRsj5v+ZN89byz5Ep/wqVgnXW7NZ0fGs
1aFwVr4NUmGVxlhwayO7wYDlXcZLg8IsxTun1wIDAQABAoIBAQCqcGV47/V504WI
UhzuBAfiF3riSPl2RQIaTGSHuauZK34GLOQ7QMwVwTiwTfqbl9AJ9x5yiJzPXukH
8SAgiGA10S9rc3hkyXVF6k4wibOHuaZIj9yAyQttvcwQWEOYowLTISqJQNQdgZud
adMERL2zvWczOx72ZjWzidEPfepASWTFSdcu6PPun05pdH+tS7mf4TRSNvBRo1St
mB8aSQ7YygTEHd/mv7C81TAE4Ai+x+UfilD9A3FnTTLA9UKG14ydOgH/nLm6wP/g
2d5OztOX3oxcNReyGzpQ0PCc7F2QLE8d4fi46eo6urhEe0PygcpzIf7LgNnYFG+o
+3QmiHABAoGBAPX4O1tXRZsDRMme6Y8YCyah+MZVAsaNT7yRt7P+ggyHvN07FQVC
0jPC/YJyQbt8pgwehdVj8R+Ba/xSvWWzRnvnOCFlSs/nJbDApKOu3csGcyFoLwmS
PB0c7R3JP8zQghmEPhE+bU5oFnSq6f5/OkvZ5m5E7tJi6EU3doNYPNonAoGBAMpI
QaaNQdy+2Ni5Xv0idFoWlgJXGgzWqXsX4AaPywhKkYWAqDJvEtyO6eEuK2qdrZ2E
8b1s6HET65eq+KxKFkZQksteqtOHYt6qvhrvyOHTyCxJ6xDD5k3VIQCoUA1o8GYs
afT8QIT55KkxNyAySBAXUC4133LwrvPHitHBZMLRAoGASw00J1nIvlTP/2lCwMvI
U0Bm5TV5RFZH9zTLRdupmOMuCJ8HefR6qRqdOdldHzUYgWgq1ALLxx45ceNZMRfz
sMjLgr0ih69y+6y8T8mCmXldVdsxRw1c1FFINKd8lSUmHV0RhzdyKNNvzqd8AJCX
nB9fxVO5P264iq4ytj8NSk8CgYEAvHIAJyclmn55Cb33zh7RAjRUX63BaQB6N2CQ
pcxDu1+snhCzoPh1O+Av5kXa0KJAYaS+iFfzW5TRQCRZMjh689qQRsHUpudTpliu
be3A2RExRIFAagf2f7/z1Ob4XOXMP4NU2pQ9jyQ2b6CX2Hi/Ra5SVhp+JlguxJC6
tJpUQmECgYEA1spCL7ZNOQ/QmOyeWbXIiLsUdSClnxXSBZnh42YcaAWzMqmMeaqW
gedYz2yVffUrFSbNp6U4b6wYwZf5iAy5AWN5NoagPo3Qv1SkZJwXcM4tb2CZ0MVW
2o+0LvfVrP6Y5UeSo8Kzp/9Exi25UuAJdGInPDmFQDXf+Tq5VxV5HoI= 
-----END RSA PRIVATE KEY-----
 
 
)KEY";
