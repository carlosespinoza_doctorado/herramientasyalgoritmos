/*
 Name:		ProyectoIoT.ino
 Created:	25/04/2022 9:39:28
 Author:	Mtro. Tawny Nava Garcia y Mtro. Carlos Espinoza
*/
#include "WiFi.h"
#include "ThingSpeak.h"
#include "WiFi.h"
#include "DHT.h"
#include "Adafruit_BMP085.h"
#include <HTTPClient.h>


String urlServidor = "http://apiherramientasalgortimos.azurewebsites.net/"; //URL de la API
unsigned long channelID = 1612330;         //ID canal ThingSpeak
const char* WriteAPIKey = "9JNO13X466B2UAGB";   //Key ThingSpeak

//============DATOS A CAMBIAR================
String ubicacion = "Huichapan";   //Colocar el nombre de la localidad donde estan
String latitud = "20.371620";     //Colocar cordenadas GPS de la localidad
String longitud = "-99.645469";
String integrante = "Carlos Espinoza";  //Colocar el nombre del integrante
int numIntegrante = 6;
const char* ssid = "Hulux-MH412";       //Colocar el nombre de la RED Wifi       
const char* password = "w2hdE9VRsikc";  //Colocar la clave de la red wifi
//============================================
#define ldr		34    //Pin Photoresistencia
#define pinDHT	13	  //Pin DHT11
#define ledNoWiFi  22
#define ledWiFi    23

DHT dht1(pinDHT, DHT11);
Adafruit_BMP085 bmp;
WiFiClient cliente;

//Variables
int luminosidad = 0;
float temp = 0;
float hum = 0;

void setup() {
	Serial.begin(115200);
	pinMode(ledNoWiFi, OUTPUT);
	pinMode(ledWiFi, OUTPUT);
	ConectarWiFi();
	ThingSpeak.begin(cliente);
}

void ConectarWiFi() {
	Serial.println("Conectando a " + String(ssid));
	WiFi.begin(ssid, password);
	digitalWrite(ledNoWiFi, HIGH);
	digitalWrite(ledWiFi, LOW);
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}
	Serial.println("Wifi conectado!");
	digitalWrite(ledNoWiFi, LOW);
	digitalWrite(ledWiFi, HIGH);
}

void loop() {
	if (WiFi.status() != WL_CONNECTED) {
		ConectarWiFi();
	}
	LeerSensores();
	ImprimirDatosDeSensores();
	CargarDatos();
	delay(25000);
}

void ImprimirDatosDeSensores() {
	Serial.println("Luminosidad: " + String(luminosidad) + " %");
	Serial.println("Temperatura: " + String(temp) + " C");
	Serial.println("Humedad: " + String(hum) + " %");
}

void LeerSensores() {
	luminosidad = map(analogRead(ldr),0,4095,0,100);
	temp = dht1.readTemperature();
	hum = dht1.readHumidity();

	while (isnan(temp) || isnan(hum)) {
		Serial.println("Lectura fallida en el sensor DHT11, repitiendo lectura...");
		delay(2000);
		temp = dht1.readTemperature();
		hum = dht1.readHumidity();
	}
}

void CargarDatos() {
	CargarATinkSpeak();
	InsertarLectura();
}


bool InsertarLectura() {
	HTTPClient http;
	String path = urlServidor + "api/Lectura";
	String json = "{\"integrante\": \"" + integrante + "\",\"ubicacion\": \"" + ubicacion + "\",\"latitud\": \"" + latitud + "\",\"longitud\": \"" + longitud + "\",\"temperatura\": " + String(temp) + ",\"humedad\": " + String(hum) + ",\"luminosidad\": " + String(luminosidad) + "}";
	http.begin(path);
	http.addHeader("Content-Type", "application/json");
	int httpResponseCode = http.POST(json);
	String respuesta = http.getString();
	http.end();
	if (httpResponseCode == 200) {
		Serial.println("Datos enviados correctamente a la API");
	}
	else {
		Serial.println("Error al cargar los datos a la API");
		Serial.println(respuesta);
	}
}

void CargarATinkSpeak() {
	ThingSpeak.setField(1, temp);
	ThingSpeak.setField(2, hum);
	ThingSpeak.setField(3, numIntegrante); 
	ThingSpeak.setField(4, luminosidad); 
	ThingSpeak.writeFields(channelID, WriteAPIKey);
	Serial.println("Datos enviados a ThingSpeak!");
}